package com.org.main.feignclient;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.org.main.model.BankTransaction;


//@FeignClient(name="BANKING-SERVICE" , url="localhost:8080")
@FeignClient(name="BANKING-SERVICE")
@RibbonClient(name="BANKING-SERVICE")
public interface IBankingServiceProxy {
	
	@GetMapping("/banking/validateCardNumber/{cardnumber}")
	public boolean validateCardNumber(@PathVariable("cardnumber") Long cardnumber);
	
	@GetMapping("/transaction/makePayment/{cardnumber}/{amntcharged}")
	public BankTransaction makePayment(@PathVariable("cardnumber") Long cardnumber , @PathVariable double amntcharged);	

}

package com.org.main.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.main.model.Slot;

@Repository
public interface SlotRepository extends JpaRepository<Slot , Long>{
	
	@Query(value = "select * from slot where vehiclecategory = ?1 and vacant='yes'", nativeQuery = true)
	List<Slot> findByCategory(int category);	

}

package com.org.main.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.main.model.Parking;
import com.org.main.model.Slot;

@Repository
public interface ParkingRepository extends JpaRepository<Parking ,Long>{	
	
	@Query(value = "select * from parking where vehiclenumber = ?1", nativeQuery = true)
	Parking findByVehicleNumber(String vehiclenumber);

}

package com.org.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.main.model.BankTransaction;
import com.org.main.model.Parking;
import com.org.main.service.ParkingService;

@RestController
@RequestMapping("/parking")
public class ParkingController {
	
	@Autowired
	ParkingService parkServ;
	
	@GetMapping("")
	public List<Parking> getParkingInfo() {
		return parkServ.findAll();
	}
	
	@PostMapping("")
	public Parking allotParking(@RequestBody Parking parking) {		
		return parkServ.save(parking);
	}
	
	@GetMapping("/exitParking/{vehiclenumber}")
	public Parking exitParking(@PathVariable String vehiclenumber) {
		return parkServ.exitParking(vehiclenumber);
	}
	
	@GetMapping("/makePayment/{vehiclenumber}")
	public BankTransaction makePayment(@PathVariable String vehiclenumber) {
		return parkServ.makePayment(vehiclenumber);
	}
	
	
}

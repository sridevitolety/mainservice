package com.org.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.main.model.Vehicle;
import com.org.main.service.VehicleService;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {
	
	@Autowired
	VehicleService vserv;
	
	@GetMapping("")
	public List<Vehicle> getVehicles(){
		return vserv.findAll();
	}

}

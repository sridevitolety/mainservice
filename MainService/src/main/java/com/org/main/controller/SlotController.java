package com.org.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.main.model.Slot;
import com.org.main.model.Vehicle;
import com.org.main.service.SlotService;

@RestController
@RequestMapping("/slot")
public class SlotController {
	
	@Autowired
	SlotService sserv;
	
	@GetMapping("")
	public List<Slot> getSlots(){		
		return sserv.findAll();
	}
	
	@GetMapping("/vehiclecategory/{category}")
	public List<Slot> getSlotsByVehicleCategory(@PathVariable int category){		
		return sserv.findByCategory(category);
	}

}

package com.org.main.service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.main.exceptions.InvalidCardNumberException;
import com.org.main.feignclient.IBankingServiceProxy;
import com.org.main.model.BankTransaction;
import com.org.main.model.Category;
import com.org.main.model.Parking;
import com.org.main.model.Slot;
import com.org.main.repository.CategoryRepository;
import com.org.main.repository.ParkingRepository;
import com.org.main.repository.SlotRepository;

@Service
public class ParkingService {
	
	@Autowired
	ParkingRepository prepo;
	
	@Autowired
	SlotRepository srepo;
	
	@Autowired
	CategoryRepository crepo;
	
	@Autowired
	IBankingServiceProxy bankingServ;
	
	public List<Parking> findAll(){
		return prepo.findAll();
	}

	public Parking save(Parking parking) {
		// TODO Auto-generated method stub
		
		boolean result = bankingServ.validateCardNumber(parking.getCardnumber());
		if(result) {
			parking.setIntime((new Date().getTime())+"");
			parking.setParkeddate(new Date());
			parking.setParkeddate(new Date());
			parking.setIntimeinmilli(Calendar.getInstance().getTimeInMillis());
			parking.setOuttimeinmilli(Calendar.getInstance().getTimeInMillis());
			
			//Generate token
			LocalDate currDate = LocalDate.now();
			int month = currDate.getMonthValue();
			int day = currDate.getDayOfYear();
			int year = currDate.getYear();	
			String token = month+""+day+""+year+""+parking.getVehiclenumber();
			parking.setToken(token);
			
			//Change status of slot;
			Optional<Slot> sl = srepo.findById(parking.getSlotid());
			Slot slot = sl.get();
			slot.setVacant("no");
			srepo.save(slot);
			//
			return prepo.save(parking);
		}else {
			throw new InvalidCardNumberException("No account exists with cardnumber  :::  "+parking.getCardnumber());
		}
	}

	public Parking exitParking(String vehiclenumber) {
		
		//get vehicle category from parking
		Parking park = prepo.findByVehicleNumber(vehiclenumber);
		//get chargeperhe from category tables
		//calculate amnt to be charged
		System.out.println("VehicleCatg :::"+park.getVehiclecategory());
		Category catg = crepo.findChargePerhr(park.getVehiclecategory());
		System.out.println("Catg :::"+catg);
		//return with exit time , amnttobe charged
		long intime = park.getIntimeinmilli();
		long outtime = Calendar.getInstance().getTimeInMillis();
		System.out.println("intime :::"+intime +"Outtime  :::"+outtime);
		long diff = outtime - intime ;
		long diffHours = diff / (60 * 60 * 1000);
		if(diffHours == 0)
			diffHours = 1;
		System.out.println("Duration :::"+diffHours);
		System.out.println("catg.getChargeperhr() :::"+catg.getChargeperhr());
		double amntcharged = diffHours * catg.getChargeperhr();
		System.out.println("AmntCharged :::"+amntcharged);
		//change status in slot to available
		park.setOuttime((new Date().getTime())+"");
		park.setAmntcharged(amntcharged);		
		System.out.println("AmntCharged :::"+park.getAmntcharged());
		
		Optional<Slot> sl = srepo.findById(park.getSlotid());
		Slot slot = sl.get();
		slot.setVacant("yes");
		srepo.save(slot);
		
		prepo.save(park);
		return park;
	}
	
	public BankTransaction makePayment(String vehiclenumber) {
		
		//make feign call to banking service , 
		Parking park = prepo.findByVehicleNumber(vehiclenumber);
		bankingServ.makePayment(park.getCardnumber() , park.getAmntcharged());
		//insert into transaction table and return that
		//deduct from bank balance
		
		return null;
	}

}

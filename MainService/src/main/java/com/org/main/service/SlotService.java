package com.org.main.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.main.model.Slot;
import com.org.main.model.Vehicle;
import com.org.main.repository.SlotRepository;

@Service
public class SlotService {
	
	@Autowired
	SlotRepository srep;
	
	public List<Slot> findAll(){		
		return srep.findAll();
	}

	public List<Slot> findByCategory(int category) {
		return srep.findByCategory(category);		
		
	}

}

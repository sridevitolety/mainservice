package com.org.main.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.main.model.Vehicle;
import com.org.main.repository.VehicleRepository;

@Service
public class VehicleService {
	
	@Autowired
	VehicleRepository vrepo;
	
	public List<Vehicle> findAll(){
		return vrepo.findAll();
	}

}

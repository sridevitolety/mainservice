package com.org.main.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Vehicle {

	@Id
	@GeneratedValue
	private long id;
	
	public Vehicle() {}
	public Vehicle(long id, String vehiclenumber, int vehiclecategory) {
		super();
		this.id = id;
		this.vehiclenumber = vehiclenumber;
		this.vehiclecategory = vehiclecategory;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getVehiclenumber() {
		return vehiclenumber;
	}
	public void setVehiclenumber(String vehiclenumber) {
		this.vehiclenumber = vehiclenumber;
	}
	public int getVehiclecategory() {
		return vehiclecategory;
	}
	public void setVehiclecategory(int vehiclecategory) {
		this.vehiclecategory = vehiclecategory;
	}
	private String vehiclenumber;
	private int vehiclecategory;
	
	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", vehiclenumber=" + vehiclenumber + ", vehiclecategory=" + vehiclecategory + "]";
	}
}

package com.org.main.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Category {
	
	@Id
	@GeneratedValue
	private long id;
	private String vehiclecategory;
	private double chargeperhr;
	public double getChargeperhr() {
		return chargeperhr;
	}
	public void setChargeperhr(double chargeperhr) {
		this.chargeperhr = chargeperhr;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getVehiclecategory() {
		return vehiclecategory;
	}
	public void setVehiclecategory(String vehiclecategory) {
		this.vehiclecategory = vehiclecategory;
	}
	public Category() {}
	public Category(long id, String vehiclecategory, double chargeperhr) {
		super();
		this.id = id;
		this.vehiclecategory = vehiclecategory;
		this.chargeperhr = chargeperhr;
	}
	@Override
	public String toString() {
		return "Category [id=" + id + ", vehiclecategory=" + vehiclecategory + ", chargeperhr=" + chargeperhr + "]";
	}
	

}

package com.org.main.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Slot {
	
	@Id
	@GeneratedValue
	private long id;
	private int vehiclecategory;
	private String vacant;
	private double chargeperhr;
	public Slot() {}
	public Slot(long id, int vehiclecategory, String vacant, double chargeperhr) {
		super();
		this.id = id;
		this.vehiclecategory = vehiclecategory;
		this.vacant = vacant;
		this.chargeperhr = chargeperhr;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getVehiclecategory() {
		return vehiclecategory;
	}
	public void setVehiclecategory(int vehiclecategory) {
		this.vehiclecategory = vehiclecategory;
	}
	public String getVacant() {
		return vacant;
	}
	public void setVacant(String vacant) {
		this.vacant = vacant;
	}
	public double getChargeperhr() {
		return chargeperhr;
	}
	public void setChargeperhr(double chargeperhr) {
		this.chargeperhr = chargeperhr;
	}	

}

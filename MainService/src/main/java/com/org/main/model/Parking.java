package com.org.main.model;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Parking {

	@Id
	@GeneratedValue
	private long id;
	private int vehiclecategory;
	
	private long contactnumber;
	private long cardnumber;
	private double amntcharged;
	private long intimeinmilli;
	private long outtimeinmilli;
	private long slotid;
	private Date parkeddate;
	private String intime;
	private String outtime;
	private String vehiclenumber;
	private String token;

	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public long getSlotid() {
		return slotid;
	}
	public Date getParkeddate() {
		return parkeddate;
	}
	public void setParkeddate(Date parkeddate) {
		this.parkeddate = parkeddate;
	}
	public long getContactnumber() {
		return contactnumber;
	}
	public void setSlotid(long slotid) {
		this.slotid = slotid;
	}
	
	


	
	public Parking(long id, int vehiclecategory, long contactnumber, long cardnumber, double amntcharged,
			long intimeinmilli, long outtimeinmilli, long slotid, Date parkeddate, String intime, String outtime,
			String vehiclenumber, String token) {
		super();
		this.id = id;
		this.vehiclecategory = vehiclecategory;
		this.contactnumber = contactnumber;
		this.cardnumber = cardnumber;
		this.amntcharged = amntcharged;
		this.intimeinmilli = intimeinmilli;
		this.outtimeinmilli = outtimeinmilli;
		this.slotid = slotid;
		this.parkeddate = parkeddate;
		this.intime = intime;
		this.outtime = outtime;
		this.vehiclenumber = vehiclenumber;
		this.token = token;
	}
	public String getIntime() {
		return intime;
	}
	public void setIntime(String intime) {
		this.intime = intime;
	}
	public String getOuttime() {
		return outtime;
	}
	public void setOuttime(String outtime) {
		this.outtime = outtime;
	}
	public int getVehiclecategory() {
		return vehiclecategory;
	}
	public void setVehiclecategory(int vehiclecategory) {
		this.vehiclecategory = vehiclecategory;
	}
	public Parking() {}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getVehiclenumber() {
		return vehiclenumber;
	}
	public void setVehiclenumber(String vehiclenumber) {
		this.vehiclenumber = vehiclenumber;
	}
	
	
	public void setContactnumber(long contactnumber) {
		this.contactnumber = contactnumber;
	}
	public long getCardnumber() {
		return cardnumber;
	}
	public void setCardnumber(long cardnumber) {
		this.cardnumber = cardnumber;
	}
	public double getAmntcharged() {
		return amntcharged;
	}
	public void setAmntcharged(double amntcharged) {
		this.amntcharged = amntcharged;
	}
	public long getIntimeinmilli() {
		return intimeinmilli;
	}
	public void setIntimeinmilli(long intimeinmilli) {
		this.intimeinmilli = intimeinmilli;
	}
	public long getOuttimeinmilli() {
		return outtimeinmilli;
	}
	public void setOuttimeinmilli(long outtimeinmilli) {
		this.outtimeinmilli = outtimeinmilli;
	}
		
}

package com.org.main.config;

import org.hibernate.cache.spi.support.AbstractReadWriteAccess.Item;
import org.springframework.batch.item.ItemProcessor;

import com.org.main.model.Slot;

public class MenuItemProcessor implements ItemProcessor<Slot ,Slot>{
	
	@Override
	  public Slot process(final Slot slot) throws Exception {
		final int vehiclecategory = slot.getVehiclecategory();
	    final String vacant = slot.getVacant();    	   
	    final double chargeperhr = slot.getChargeperhr();
	    

	    final Slot processedItem = new Slot(1L,vehiclecategory,vacant,chargeperhr);
	    return processedItem;
	  }

}

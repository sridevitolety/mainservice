package com.org.main.config;

import javax.sql.DataSource;

import org.hibernate.cache.spi.support.AbstractReadWriteAccess.Item;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.org.main.model.Slot;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	  @Autowired
	  public JobBuilderFactory jobBuilderFactory;

	  @Autowired
	  public StepBuilderFactory stepBuilderFactory;
	  
	  @Autowired
	  DataSource datasource;
	  
	  @Bean
	  public FlatFileItemReader<Slot> reader() {
	    return new FlatFileItemReaderBuilder<Slot>()
	      .name("UserItemReader")
	      .resource(new ClassPathResource("data.csv"))
	      .delimited()
	      .names(new String[]{"vehiclecategory","vacant","chargeperhr"})
	      .fieldSetMapper(new BeanWrapperFieldSetMapper<Slot>() {{
	        setTargetType(Slot.class);
	      }})
	      .build();
	  }

	  @Bean
	  public MenuItemProcessor processor() {
	    return new MenuItemProcessor();
	  }

	  @Bean
	  public JdbcBatchItemWriter<Slot> writer(DataSource dataSource) {
	    return new JdbcBatchItemWriterBuilder<Slot>()
	      .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
	      .sql("INSERT INTO slot (vehiclecategory,vacant ,chargeperhr) VALUES (:vehiclecategory, :vacant, :chargeperhr)")
	      .dataSource(dataSource)
	      .build();
	  }
	  
	  @Bean
	  public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
	    return jobBuilderFactory.get("importUserJob")
	      .incrementer(new RunIdIncrementer())
	      .listener(listener)
	      .flow(step1)
	      .end()
	      .build();
	  }

	  @Bean
	  public Step step1(JdbcBatchItemWriter<Slot> writer) {
	    return stepBuilderFactory.get("step1")
	      .<Slot, Slot> chunk(10)
	      .reader(reader())
	      .processor(processor())
	      .writer(writer)
	      .build();
	  }


}

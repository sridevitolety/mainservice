DROP TABLE IF EXISTS item;

CREATE TABLE item (
id INT AUTO_INCREMENT PRIMARY KEY,
vendorname varchar(250) not null,
itemcode VARCHAR(250) NOT NULL,
itemname VARCHAR(250) NOT NULL,
  priceperunit number not null
);

DROP TABLE IF EXISTS slot;

CREATE TABLE slot (
id INT AUTO_INCREMENT PRIMARY KEY,
vehiclecategory number not null,
vacant VARCHAR(5) NOT NULL,
chargeperhr number not null
);

insert into parking(id , vehiclenumber,intime,outtime,intimeinmilli,outtimeinmilli,contactnumber,cardnumber, vehiclecategory, slotid , amntcharged,parkeddate, token) values(1, 'TS07EK2736' ,'2020-05-04 10:55:29.924','2020-05-04 11:55:29.924',1588580971352,1588582971352,9999999999,1212121212121212,1, 1,0.00,sysdate,'abcdef');



insert into category(id, chargeperhr , vehiclecategory)  values(1, 10.00 , 1);
insert into category(id, chargeperhr , vehiclecategory)  values(2, 25.00 , 2);